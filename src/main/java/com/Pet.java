package com;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class Pet {
    private Species species;
    String nickname;
    int age;
    int trickLevel;
    ArrayList<String> habits = new ArrayList<>();


    public Pet(int age) {
        this.age = age;
    }

    public void eat() {
        System.out.println("I am eating");

    }

    public Species getSpecies() {

        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public abstract void respond();

    public abstract void foul();


    @Override
    public String toString() {
        return "dog{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits + '}' +
                ",where dog=" + species;
    }

    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(ArrayList<String> habits) {
        this.habits = habits;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, ArrayList<String> habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
}


