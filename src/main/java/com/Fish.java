package com;

public class Fish extends Pet {
    @Override
    public void respond() {
        System.out.println("Hello,Fish owner.I am " + nickname + ".I miss you!");

    }

    @Override
    public void foul() {
        System.out.println("Hello,Foul from Fish");

    }
}
